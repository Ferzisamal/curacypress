
describe("All Negative Case", function(){

    it("No Username",() =>{
        cy.visit('/')
        cy.viewport(1920,1020)
        cy.get('#menu-toggle > .fa').click()
        cy.contains('Login').click()

        cy.get('.form-horizontal') //validation that we are in login page

        cy.get('#txt-username').type(" ")
        cy.get('#txt-password').type("ThisIsNotAPassword")
        cy.get('#btn-login').click()

        cy.contains('Logout').should('not.exist')


    })

    it("No Password",() =>{
        cy.visit('https://katalon-demo-cura.herokuapp.com/')
        cy.viewport(1920,1020)
        cy.get('#menu-toggle > .fa').click()
        cy.contains('Login').click()

        cy.get('.form-horizontal') //validation that we are in login page

        cy.get('#txt-username').type("John Doe")
        cy.get('#txt-password').type(" ")
        cy.get('#btn-login').click()

        cy.contains('Logout').should('not.exist')


    })

    it("Invalid Username",() =>{
        cy.visit('https://katalon-demo-cura.herokuapp.com/')
        cy.viewport(1920,1020)
        cy.get('#menu-toggle > .fa').click()
        cy.contains('Login').click()

        cy.get('.form-horizontal') //validation that we are in login page

        cy.get('#txt-username').type("Andre Dufreen")
        cy.get('#txt-password').type("ThisIsNotAPassword")
        cy.get('#btn-login').click()

        cy.contains('Logout').should('not.exist')


    })

    it("Invalid Password",() =>{
        cy.visit('https://katalon-demo-cura.herokuapp.com/')
        cy.viewport(1920,1020)
        cy.get('#menu-toggle > .fa').click()
        cy.contains('Login').click()

        cy.get('.form-horizontal') //validation that we are in login page

        cy.get('#txt-username').type("John Doe")
        cy.get('#txt-password').type("thisnitapassword")
        cy.get('#btn-login').click()

        cy.contains('Logout').should('not.exist')


    })
})

describe("Pos Case", function(){

    it("Valid credentials",() =>{
        cy.visit('https://katalon-demo-cura.herokuapp.com/')
        cy.viewport(1920,1020)
        cy.get('#menu-toggle > .fa').click()
        cy.contains('Login').click()

        cy.get('.form-horizontal') //validation that we are in login page

        cy.get('#txt-username').type("John Doe")
        cy.get('#txt-password').type("ThisIsNotAPassword")
        cy.get('#btn-login').click()

        cy.contains('Logout').should('exist')


    })

    
})